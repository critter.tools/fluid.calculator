# utilities
get_by_id = (id) ->
    document.getElementById(id)

on_click = (obj, func) ->
    obj.addEventListener('click', func)

on_input = (obj, func) ->
    obj.addEventListener('input', func)

on_focusout = (obj, func) ->
    obj.addEventListener('focusout', func)

round = (number, n_decimals=0) ->
    scale = 10 ** n_decimals
    return Math.round((number + Number.EPSILON) * scale) / scale

str = (obj, trim=true) ->
    return String(obj).trim()

setter = (element, attr, val) ->
    element.setAttribute(attr, val)

print = (obj) ->
    console.log(obj)

# inputs
timeframe =   get_by_id('timeframe-hours')
weight =      get_by_id('weight')
dehydration = get_by_id('dehydration')
fluid_needs = get_by_id('fluid-needs')
timing =      get_by_id('timing')
inputs = [weight,
          dehydration,
          fluid_needs,
          timeframe]

# labels
weight_label =        get_by_id('weight-label')
dehydration_label =   get_by_id('dehydration-label')
fluid_needs_label =   get_by_id('fluid-usage-label')
timeframe_label =     get_by_id('timeframe-label')
initial_fluid_label = get_by_id('initial-fluid-label')

# buttons
weight_kg_btn =    get_by_id('weight-kg-button')
weight_grams_btn = get_by_id('weight-grams-button')
weight_mg_btn =    get_by_id('weight-mg-button')
weight_unit_btns = [weight_kg_btn,
                    weight_grams_btn,
                    weight_mg_btn]

# outputs
total_fluid =       get_by_id('total-fluid')
daily_maintenance = get_by_id('daily-maintenance')
deficit =           get_by_id('deficit')
subcut =            get_by_id('subcut')
remaining_fluid =   get_by_id('remaining-fluid')
output_boxes = [total_fluid.parentElement,
                daily_maintenance.parentElement,
                deficit.parentElement,
                subcut.parentElement,
                remaining_fluid.parentElement]

# other
timeframe_copy = get_by_id('timeframe-copy')

# weight units
weight_units = 'kg'

weight_conversions = 
    {'kg-grams': 1e3,
    'kg-mg': 1e6,
    'grams-kg': 1e-3,
    'grams-mg': 1e3,
    'mg-kg': 1e-6,
    'mg-grams': 1e-3}

weight_unit_click = (event) ->
    button = event.target
    new_units = str(button.textContent)
    for btn in weight_unit_btns
        btn.disabled = if btn is button \
                          then true \
                          else false
    conversion = "#{str(weight_units)}-#{new_units}"
    weight.value = weight.value * weight_conversions[conversion]
    weight_units = new_units

for btn in weight_unit_btns
    on_click(btn, weight_unit_click)

# conversions
to_kg = (mass) ->
    if weight_units isnt 'kg'
        conversion = "#{str(weight_units)}-kg"
        mass = mass * weight_conversions[conversion]
    return mass

to_grams = (mass) ->
    if weight_units isnt 'grams'
        conversion = "#{str(weight_units)}-grams"
        mass = mass * weight_conversions[conversion]
    return mass

# calculations
fluid_deficit = () ->
    weight_grams = to_grams(weight.value)
    deficit_ml = weight_grams * dehydration.value / 100
    return deficit_ml

hourly_maintenance_dose = () ->
    weight_kg = to_kg(weight.value)
    return weight_kg * fluid_needs.value

total_fluid_dose = () ->
    maintenance = timeframe.value * hourly_maintenance_dose()
    deficit_ml = fluid_deficit()
    return maintenance + deficit_ml

fluid_per_hour = () ->
    return total_fluid_dose() / timeframe.value

daily_maintenance_dose = () ->
    return hourly_maintenance_dose() * 24

subcut_dose = () ->
    return 0.05 * to_grams(weight.value)

total_remaining_fluid = () ->
    return total_fluid_dose() - subcut_dose()

# updating
stringify = (number, n_digits=4) ->
    return str(number.toString().slice(0, n_digits + 1))

highlight_change = (element, new_val) ->
    new_txt = stringify(new_val)
    if str(element.textContent) != new_txt
        box = element.parentElement
        if box.className == 'ouput-box'
            box.classList.add('pulse')
        else
            box.classList.add('fade')

        element.textContent = new_txt
     
reset_anim = (event) ->
    event.target.classList.remove('pulse')
    event.target.classList.remove('fade')

for box in output_boxes
    box.addEventListener('animationend', reset_anim)

copy_timeframe = () ->
    timeframe_copy.textContent = "(over #{timeframe.value} hours)"

update_outputs = (event) ->
    event.target._prev_value = event.target.value
    highlight_change(total_fluid, total_fluid_dose())
    highlight_change(daily_maintenance, daily_maintenance_dose())
    highlight_change(deficit, fluid_deficit())
    highlight_change(subcut, subcut_dose())
    highlight_change(remaining_fluid, total_remaining_fluid())
    copy_timeframe()
    return

# input
clear_input = (event) ->
    target = event.target
    target._prev_value = target.value
    target.value = ""
    return

restore_input = (event) ->
    target = event.target
    target.value = target._prev_value
    return

for field in inputs
    on_click(field, clear_input)
    on_input(field, update_outputs)
    on_focusout(field, restore_input)
